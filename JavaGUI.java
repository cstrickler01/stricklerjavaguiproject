import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;
//import com.sun.corba.se.spi.orbutil.fsm.Action;

public class JavaGUI extends JFrame {
    JButton fileButton = new JButton( "Browse Files");
    JButton run = new JButton("Run"); 
    public JavaGUI() {
        this.setTitle("Strickler Java GUI Project");
        this.setBounds( 200, 300, 250, 250);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
       

        this.fileButton.setBounds(50, 50, 120, 30);
        this.getContentPane().add(fileButton);
        this.fileButton.addActionListener( new FileButtonListener());
    }   
    
    private class FileButtonListener implements ActionListener {
        @Override
        public void actionPerformed( ActionEvent e) {
           // StricklerJavaProj st = new StricklerJavaProj();
            if(e.getSource() == fileButton){
                try{
                    StricklerJavaProj.chooseFile();
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }
        
    }
}