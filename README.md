# What's in this folder:
This folder contains the source file for a Java program that counts the total number of words in a .txt file, and how many times each word appears.

# User instructions:
Run the source file and select the .txt file you would like to run through the program.