import java.io.*;
import java.lang.*;
import java.util.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import javafx.stage.FileChooser;

//import java.awt.*;

/*
* This program counts the total words in a text file, how many unique words
* there are, and how many times each unique word appears in the file.
* @author Chase Strickler
*/
public class StricklerJavaProj {
    /*
    * The main function is used to call the hash function, then the sort function
    * @param args is used to take the desired text file name from the command line
    */
    public static void main(String[] args) {     
        JavaGUI gui = new JavaGUI();
        gui.setVisible(true);
        
    }

    public static void chooseFile() throws IOException {
        Map<String, Integer> hm = new HashMap<String, Integer>(); // Make the hash map.

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        java.io.File file = null;
        if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
            file = fileChooser.getSelectedFile();
            hash(hm, file);
        }
        else{
            System.out.println("No file selected");
            System.exit(1);
        }
    }

    /*
    * This function will put the word from the file into the hash map
    * @param s is the word being added to the hash map
    */
    public static void hash(Map<String, Integer> hm, File f) throws FileNotFoundException { 
        Scanner sc = null;
        sc = new Scanner(f);
        int total_words = 0;

        // This loop strips a string of its punctuation, then puts the stripped string into the hashmap
        while(sc.hasNext()){
            String str = sc.next();
            str = str.toLowerCase();

            // Replace all punctuation using regex
            String result = str.replaceAll("[^a-z]+", "");
            Integer count = hm.get(result);

            // Put the string in the hash map. ? is ternary op, shorthand if-else. 
            // If the word hasn't appeared, add it and set value to one
            // If it has, increase its count
            hm.put(result, (count == null) ? 1 : count + 1); 
            total_words++;
        }
        
        // Close file
        sc.close();                                            
        
        printResults(sortByKey(hm), total_words);
        }

    /*
    * This function uses a TreeMap to sort the hashmap alphabetically
    * @param Map <K,V> hm is the hashmap to be sorted
    */ 
    public static TreeMap sortByKey(Map<String, Integer> hm) {

        // Create a new TreeMap called sorted
        TreeMap<String, Integer> sorted = new TreeMap<>();

        // Copy data from the hash map into the TreeMap, which will sort the map alphabetically
        sorted.putAll(hm);
        return(sorted);

    }


    /*
    * This method simply prints out the contents of the map.
    * @param formatedMap is the alphabetically sorted TreeMap containing the words in the file
    */
    public static void printResults(TreeMap<String, Integer> formatedMap, Integer total_words) throws FileNotFoundException{
        // print alphabetically sorted TreeMap, which, because it's a Red-Black Tree implementation, is naturally sorted
        // Print the total words and total unique words, then print the hashmap
        System.out.println("Output printed to output.txt");

        PrintWriter writer = new PrintWriter("output.txt");
        //write to file
        writer.print("Total Words: " + total_words + '\t');
        writer.println("Total Unique Words: " + formatedMap.size());
        writer.println("-----------------------------------------------------------");

        for(Map.Entry<String, Integer> entry : formatedMap.entrySet()){
          writer.println("The word: " + entry.getKey() 
              + " appears " + entry.getValue() + " times.");
        }

        writer.close();
    }
}